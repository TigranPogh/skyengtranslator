#  Skyeng

## Technology Stack

**Software architecture:** customized MVVM ( Model View ViewModel )



 ##  Libraries used

- `Retrofit` - Retrofit is an HTTP networking library
- `Coroutine` -  Coroutines is an Kotlin library which allow you to write asynchronous code in a sequential fashion 
- `Coil` -  Coil is an acronym for Coroutine Image Loader
- `Koin` -  Koin is a lightweight dependency injection framework
- `RecyclerView` - RecyclerView is a ViewGroup added to the android studio as a successor of the GridView and ListView


