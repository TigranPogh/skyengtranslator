package com.directorial1.skyengtranslator

import android.app.Application
import com.directorial1.skyengtranslator.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin()
    }

    private fun startKoin() {
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@AppApplication)
            modules(
                listOf(
                    mainModule
                )
            )
        }
    }
}