package com.directorial1.skyengtranslator

object Network {

    const val BASE_URL = "https://dictionary.skyeng.ru/"

    const val SEARCH_END_POINT = "api/public/v1/words/search"
}