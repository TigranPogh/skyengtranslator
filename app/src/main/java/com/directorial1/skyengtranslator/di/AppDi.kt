package com.directorial1.skyengtranslator.di

import com.directorial1.skyengtranslator.viewmodel.WordsViewModel
import com.directorial1.skyengtranslator.viewmodel.repository.WordsRepository
import org.koin.core.module.Module
import org.koin.dsl.module

val mainModule : Module = module {

    single { WordsRepository() }

    single { WordsViewModel(get()) }
}