package com.directorial1.skyengtranslator.interfaces

import com.directorial1.skyengtranslator.model.WordResponse

interface WordsItemClickListener {

    fun onWordsItemClicked(item : WordResponse.MeaningsItem)
}