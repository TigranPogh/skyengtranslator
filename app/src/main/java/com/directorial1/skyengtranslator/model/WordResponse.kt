package com.directorial1.skyengtranslator.model

import com.google.gson.annotations.SerializedName

data class WordResponse(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("meanings")
	val meanings: List<MeaningsItem?>? = null
)	{

	data class MeaningsItem(
		@field:SerializedName("previewUrl")
		val previewUrl: String? = null,

		@field:SerializedName("transcription")
		val transcription: String? = null,

		@field:SerializedName("partOfSpeechCode")
		val partOfSpeechCode: String? = null,

		@field:SerializedName("imageUrl")
		val imageUrl: String? = null,

		@field:SerializedName("translation")
		val translation: Translation? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("soundUrl")
		val soundUrl: String? = null
	)

	data class Translation(
		@field:SerializedName("note")
		val note: String? = null,

		@field:SerializedName("text")
		val text: String? = null
	)
}


