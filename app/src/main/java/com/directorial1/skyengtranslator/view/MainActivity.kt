package com.directorial1.skyengtranslator.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.directorial1.skyengtranslator.R
import com.directorial1.skyengtranslator.interfaces.WordsItemClickListener
import com.directorial1.skyengtranslator.model.WordResponse
import com.directorial1.skyengtranslator.view.adapter.WordsAdapter
import com.directorial1.skyengtranslator.viewmodel.WordsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(), WordsItemClickListener {

    private val wordsViewModel: WordsViewModel by inject()

    private val wordsAdapter: WordsAdapter by lazy {
        val adapter = WordsAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        return@lazy adapter
    }

    private var word = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edtWord.requestFocus()

        initObservers()

        btnTranslate.setOnClickListener {
            word = edtWord.text.toString()
            if (!TextUtils.isEmpty(word)) {
                wordsViewModel.getAllData(word)
            }

            it.hideKeyboard()
        }
    }

    private fun initObservers() {
        wordsViewModel.getAllDataLiveData().observe(this, Observer { items ->
            wordsAdapter.setItems(items)
            wordsAdapter.notifyDataSetChanged()
        })
    }

    override fun onWordsItemClicked(item: WordResponse.MeaningsItem) {
        wordsViewModel.selectItem(item)
        intent = Intent(this, WordsDetailActivity::class.java)
        intent.putExtra("key", edtWord.text.toString())
        startActivity(intent)
    }

    private fun View.hideKeyboard() {
        val inputManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }
}
