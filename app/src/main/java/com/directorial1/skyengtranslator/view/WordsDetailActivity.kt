package com.directorial1.skyengtranslator.view

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import coil.api.load
import com.directorial1.skyengtranslator.R
import com.directorial1.skyengtranslator.viewmodel.WordsViewModel
import kotlinx.android.synthetic.main.activity_words_detail.*
import org.koin.android.ext.android.inject

class WordsDetailActivity : AppCompatActivity() {

    private val wordsViewModel : WordsViewModel by inject()

    lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_words_detail)

        val bundle:Bundle = intent.extras!!
        val word = bundle.get("key")

        wordsViewModel.getSelectedItem().observe(this, Observer {
            if (it.imageUrl != "") {
                imgWord.load("https:" + it.imageUrl)
            }

            txtWordTranslation.text = it.translation?.text.toString()
            txtWord.text = word.toString()

                mediaPlayer = MediaPlayer.create(this, Uri.parse("https:" + it.soundUrl))


        })

        imgSound.setOnClickListener {
            mediaPlayer.start()
        }
    }
}
