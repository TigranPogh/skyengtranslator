package com.directorial1.skyengtranslator.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.directorial1.skyengtranslator.R
import com.directorial1.skyengtranslator.interfaces.WordsItemClickListener
import com.directorial1.skyengtranslator.model.WordResponse.MeaningsItem

class WordsAdapter(private val wordsItemClickListener: WordsItemClickListener) : RecyclerView.Adapter<WordsAdapter.WordsHolder>() {

    private lateinit var list: List<MeaningsItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordsHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.words_row, parent, false)
        return WordsHolder(
            wordsItemClickListener, v
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: WordsHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setItems(list: List<MeaningsItem>) {
        this.list = list
    }

    inner class WordsHolder(wordsItemClickListener: WordsItemClickListener, itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var wordTextView : TextView

        init {
            findViews(itemView)

            itemView.setOnClickListener {
                wordsItemClickListener.onWordsItemClicked(list[adapterPosition])
            }
        }

        private fun findViews(view : View) {
            wordTextView = view.findViewById(R.id.wordRow)
        }

        fun bind(meaningsItem: MeaningsItem) {
            wordTextView.text = meaningsItem.translation?.text.toString()
        }

    }
}