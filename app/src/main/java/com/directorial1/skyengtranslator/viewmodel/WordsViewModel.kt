package com.directorial1.skyengtranslator.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.directorial1.skyengtranslator.model.WordResponse
import com.directorial1.skyengtranslator.viewmodel.repository.WordsRepository
import kotlinx.coroutines.launch

class WordsViewModel(private val repository: WordsRepository) : ViewModel() {

    private var wordsMutableLiveData = MutableLiveData<List<WordResponse.MeaningsItem>>()

    private var selectedWordRowData = MutableLiveData<WordResponse.MeaningsItem>()

    fun getAllData(word :String) = viewModelScope.launch{
        try {
            val response = repository.getAllData(word)
            if(response.isSuccessful && response.body() != null) {
                wordsMutableLiveData.value = response.body()!![0].meanings as List<WordResponse.MeaningsItem>
            }
        } catch (ex : Exception) {

        }
    }

    fun getAllDataLiveData() : LiveData<List<WordResponse.MeaningsItem>> = wordsMutableLiveData

    fun selectItem(meaningsItem: WordResponse.MeaningsItem) {
        selectedWordRowData.value = meaningsItem
    }

    fun getSelectedItem() : LiveData<WordResponse.MeaningsItem> = selectedWordRowData
}