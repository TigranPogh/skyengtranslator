package com.directorial1.skyengtranslator.viewmodel.repository

import com.directorial1.skyengtranslator.model.WordResponse
import com.directorial1.skyengtranslator.webservice.Api
import com.directorial1.skyengtranslator.webservice.ApiClient
import retrofit2.Response

class WordsRepository {

    suspend fun getAllData(word :String) : Response<List<WordResponse>>{
        return ApiClient().getApiClient()!!.create(
            Api::class.java).getData(word)
    }
}
