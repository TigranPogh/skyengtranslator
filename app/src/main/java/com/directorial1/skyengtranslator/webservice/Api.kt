package com.directorial1.skyengtranslator.webservice

import com.directorial1.skyengtranslator.Network.SEARCH_END_POINT
import com.directorial1.skyengtranslator.model.WordResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET(SEARCH_END_POINT)
    suspend fun getData(@Query("search" )item : String) : Response<List<WordResponse>>
}